package types

import (
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

//Visitor
type Visitor struct {
	Uuid string
	Id int
	UserAgent string
	RemoteAddr string
	ClientId string
	At time.Time
}

func (visitor *Visitor) FillFromRequest (r *http.Request, uuid string) {
	//парсим FORM DATA
	_ = r.ParseForm()
	visitor.ClientId = r.Form.Get("Client-Id")
	visitor.Id, _ = strconv.Atoi(r.Form.Get("User-Id"))
	visitor.Uuid = r.Form.Get("Authorization")

	//парсим заголовки
	//userAgent
	visitor.UserAgent = strings.Join(r.Header["User-Agent"], ";")
	//remoteAddr
	visitor.RemoteAddr, _, _ = net.SplitHostPort(r.RemoteAddr)
	//uuid
	if uuid != "" && visitor.Uuid == "" {
		visitor.Uuid = uuid
	}
	visitor.At = time.Now()
}

//Shop
type Shop struct {
	Id int `json:"shopId"`
}

//Product
type Product struct {
	Id int `json:"productId"`
	Shop
	Price
}

//Price
type Price struct {
	Value float32 `json:"price"`
	OldValue float32 `json:"oldPrice"`
}