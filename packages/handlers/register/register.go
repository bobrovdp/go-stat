package register

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"go.srv/packages/db/clickhouse"
	"go.srv/packages/db/redis"
	"go.srv/packages/types"
	"log"
	"net/http"
	"strconv"
	"time"
)

var Rows = make([]types.Visitor, 0)
var OrderLimit = 5
var Channel = make(chan types.Visitor, OrderLimit)
var expirationTime, _ = time.ParseDuration("24h")

func Handler (w http.ResponseWriter, r *http.Request) {


	//генерируем uuid
	uuid, err := randomHex(128)

	//создаём структуру посетителя
	var visitor types.Visitor
	visitor.FillFromRequest(r, uuid)

	jsonVisitor, err := json.Marshal(visitor)
	if err != nil {
		fmt.Println(err)
	}

	//записываем в redis данные о регистрации
	go redis.Client.Set(uuid, jsonVisitor, expirationTime)

	Channel <- visitor

	w.Header().Set("Access-Token", uuid)
	w.Header().Set("Expires-In", strconv.FormatInt(int64(expirationTime.Seconds()), 10))
	w.Header().Set("Access-Control-Allow-Origin", "*")
}

func randomHex(n int) (string, error) {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}

func Write(rows []types.Visitor) {
	//подключаемся к кликхаус
	tx, _ := clickhouse.Connect.Begin()

	//подготавливаем запрос
	stmt, _ := tx.Prepare("insert into visitor (uuid, user_id, user_agent, remote_addr, client_id, at) VALUES (?, ?, ?, ?, ?, ?)");
	defer stmt.Close()
	for i:=0; i<len(rows); i+=1 {
		if _, err := stmt.Exec(rows[i].Uuid, rows[i].Id, rows[i].UserAgent, rows[i].RemoteAddr, rows[i].ClientId, rows[i].At); err != nil {
			fmt.Println(err)
		}
	}
	if err := tx.Commit(); err != nil {
		log.Fatal(err)
	}
}