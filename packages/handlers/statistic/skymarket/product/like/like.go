package product_like

import (
	"encoding/json"
	"fmt"
	"go.srv/packages/types"
	"net/http"
	"strconv"
	"time"
)

type Row struct {
	types.Visitor
	types.Product
	Action string `json:"action"`
}

type RowFilter struct {
	Row
	time.Time `json:"timeFrom"`
	time.Duration `json:"timeDuration"`
}


var OrderLimit = 100

var Rows = make([]Row, 0)

var Channel = make(chan Row, 1000)

func Handler (w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		queryValues := r.URL.Query()
		shopIdStr := queryValues.Get("shopId")
		if shopIdStr == "" {
			_, _ = fmt.Fprintln(w, "Invalid parameter shopId: empty parameter")
			return
		}
		shopId, err:= strconv.Atoi(shopIdStr)
		if err != nil {
			_, _ = fmt.Fprintln(w, "Invalid parameter shopId: Could not convert from string to int")
			return
		}
		var rowFilter RowFilter
		rowFilter.Product.Shop.Id = shopId
		//Read(&rowFilter, w)
		break;
	case "POST":
		decoder := json.NewDecoder(r.Body)
		//формируем Row из JSON
		var row Row
		err := decoder.Decode(&row)
		fmt.Println(row)
		if err != nil {
			switch err.Error() {
			case "EOF":
				_, _ = fmt.Fprintf(w, "Invalid request body: %v", err.Error())
				break;
			default:
				_, _ = fmt.Fprintf(w, "Internal server error: %v", err.Error())
				break;
			}
			return
		}
		Channel <- row
		break;
	default:
		_, err := fmt.Fprintf(w, "Method %v not allowed", r.Method)
		if err != nil {
			fmt.Println(err)
		}
		break;
	}
}

func Write (rows []Row) {
	/*	dbCon := db.GetConn("productLike")

	//todo: change "do nothing" to "update row"
	preparedQuery, err := dbCon.Prepare("INSERT INTO user_like_product(user_id, shop_id, product_id, price, old_price) VALUES ($1, $2, $3, $4, $5) ON CONFLICT DO NOTHING ")
	if err != nil {
		fmt.Println(err)
	}
	for i:=0; i<len(rows); i+=1 {
		_, err := preparedQuery.Exec(rows[i].Visitor.Id, rows[i].Product.Shop.Id, rows[i].Product.Id, rows[i].Product.Price.Value, rows[i].Product.Price.OldValue)
		if err != nil {
			fmt.Println(err)
		}
	}
	*/
}

func Read (rowFilter *RowFilter, w http.ResponseWriter) {
/*	dbCon := db.GetConn("productLike")

	preparedQuery, err := dbCon.Prepare("SELECT count(id) as likes, product_id as avrPrice FROM user_like_product where shop_id = $1 group by product_id")
	if err != nil {
		fmt.Println(err)
	}
	dbRes, err := preparedQuery.Exec(rowFilter.Product.Shop.Id)
	if err != nil {
		fmt.Println(err)
	}
	data, err := dbRes.RowsAffected()
	if err != nil {
		fmt.Println(err)
	}
	_, err = fmt.Fprintln(w, data)
	if err != nil {
		fmt.Println(err)
	}
	*/
}
