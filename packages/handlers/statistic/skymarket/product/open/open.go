package product_open

import (
	"encoding/json"
	"fmt"
	"go.srv/packages/db/clickhouse"
	"go.srv/packages/types"
	"log"
	"net/http"
	"time"
)

type Row struct {
	types.Visitor
	types.Product
	At time.Time
}

var OrderLimit = 10

var Rows = make([]Row, 0)

var Channel = make(chan Row, 1000)

func Handler (w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		//Забираем из контекста посетителя
		visitor := r.Context().Value("visitor").(types.Visitor)

		//формируем Row из JSON
		decoder := json.NewDecoder(r.Body)
		var row Row
		err := decoder.Decode(&row)
		if err != nil {
			switch err.Error() {
			case "EOF":
				_, _ = fmt.Fprintf(w, "Invalid request body: %v", err.Error())
				break;
			default:
				_, _ = fmt.Fprintf(w, "Internal server error: %v", err.Error())
				break;
			}
			return
		}
		row.At = time.Now()
		row.Visitor = visitor

		Channel <- row
		break;
	}
}

func Write (rows []Row) {
	//подключаемся к кликхаус
	tx, err := clickhouse.Connect.Begin()
	if err != nil {
		log.Fatal(err)
		return
	}

	//подготавливаем запрос
	stmt, _ := tx.Prepare("insert into skymarket_user_open_product (uuid, product_id, shop_id, at) values (?, ?, ?, ?)");
	defer stmt.Close()
	for i:=0; i<len(rows); i+=1 {
		if _, err := stmt.Exec(rows[i].Visitor.Uuid, rows[i].Product.Id, rows[i].Product.Shop.Id, rows[i].At); err != nil {
			fmt.Println(err)
		}
	}
	if err := tx.Commit(); err != nil {
		log.Fatal(err)
	}
}
