package auth

import (
	"fmt"
	"net/http"
)

func Handler (w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		_, _ = fmt.Fprintf(w, "Ok")
		break;
	default:
		_, err := fmt.Fprintf(w, "Method %v not allowed", r.Method)
		if err != nil {
			fmt.Println(err)
		}
		break;
	}
}