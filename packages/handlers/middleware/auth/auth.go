package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"go.srv/packages/db/redis"
	"go.srv/packages/types"
	"net/http"
)

func Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {
		//создаём структуру посетителя
		var remoteVisitor types.Visitor
		remoteVisitor.FillFromRequest(r, "")

		//загружаем пользователя по uuid из Redis
		jsonVisitor, err := redis.Client.Get(remoteVisitor.Uuid).Result()
		if err != nil {
			_, _ = fmt.Fprintln(w, "Unauthorized: token expired or invalid")
			return
		}

		//формируем структуру из json
		var savedVisitor types.Visitor
		bytesJsonVisitor := []byte(jsonVisitor)
		err = json.Unmarshal(bytesJsonVisitor, &savedVisitor)
		if err != nil {
			switch err.Error() {
			case "EOF":
				_, _ = fmt.Fprintf(w, "Invalid request body: %v", err.Error())
				break;
			default:
				_, _ = fmt.Fprintf(w, "Internal server error: %v", err.Error())
				break;
			}
			return
		}

		//сравниваем данные UserAgent и RemoteAddr и говорим о несовпадении в случае отличий
		if remoteVisitor.UserAgent != savedVisitor.UserAgent || remoteVisitor.RemoteAddr != savedVisitor.RemoteAddr || remoteVisitor.ClientId != savedVisitor.ClientId {
			_, _ = fmt.Fprintln(w, "Unauthorized: User Agent or Remote Address or Client Id mismatch")
			return
		}
		ctx := context.WithValue(r.Context(), "visitor", savedVisitor)

		next.ServeHTTP(w, r.WithContext(ctx))
		w.Header().Set("Access-Control-Allow-Origin", "*")
	})
}
