package listener

import (
	"go.srv/packages/handlers/register"
	productLike "go.srv/packages/handlers/statistic/skymarket/product/like"
	productOpen "go.srv/packages/handlers/statistic/skymarket/product/open"
)

func Write () {
	for {
		select {
		case  i := <-productOpen.Channel:
			productOpen.Rows = append(productOpen.Rows, i)
			if len(productOpen.Rows) == productOpen.OrderLimit {
				go productOpen.Write(productOpen.Rows)
				productOpen.Rows = productOpen.Rows[:0]
			}
			break
		case  i := <-productLike.Channel:
			productLike.Rows = append(productLike.Rows, i)
			if len(productLike.Rows) == productLike.OrderLimit {
				go productLike.Write(productLike.Rows)
				productLike.Rows = productLike.Rows[:0]
			}
			break
		case  i := <-register.Channel:
			register.Rows = append(register.Rows, i)
			if len(register.Rows) == register.OrderLimit {
				go register.Write(register.Rows)
				register.Rows = register.Rows[:0]
			}
			break
		default:
		}
	}
}