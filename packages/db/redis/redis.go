package redis

import (
	"fmt"
	"github.com/go-redis/redis"
)

var Client *redis.Client

func InitClient() *redis.Client {
	var key= "W8-kQ?!Ga9peFjbJhTdE%yRHV!CKGcBH#_T*Nx5V3UE6wS77_j%#X6Ye@E#KadXLcnMCBq&e$z@&vsRy^zf3g55mEB?C=uv9js%mUfr-C4gCSZt+CPRm#kP_$#7stUpG!Ywz_HfuMPw69euMCx^pAGThy*7vwdQLvw^hB5HUcNFZ34GHNMaze7*@!&7%pA?#n!ce+B?LAzjwT%aEAp8X$#4Tt#pmBJ#Mw=8=K=V8cEdz=rGKvbv2cBsejj3dUkwL"
	client := redis.NewClient(&redis.Options{
		//Addr:     "165.22.214.45:6379",
		Addr:     "127.0.0.1:6379",
		Password: key, // no password set
		DB:       0,   // use default DB
	})

	ping, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}
	fmt.Printf("Redis PING: %v\n", ping)
	return client
}
