package clickhouse

import (
	"database/sql"
	_ "github.com/kshvakov/clickhouse"
	"log"
)

var Connect *sql.DB

func InitConnection() *sql.DB {
	//connect, err := sql.Open("clickhouse", "tcp://165.22.214.45:9000?debug=true&username=default&password=123mz465q&database=stat&read_timeout=10&write_timeout=20")
	connect, err := sql.Open("clickhouse", "tcp://127.0.0.1:9000?debug=true&username=default&password=123mz465q&database=stat&read_timeout=10&write_timeout=20")
	if err != nil {
		log.Fatal(err)
	}

	if err := connect.Ping(); err != nil {
		panic(err)
	}
	return connect
}

/*
create table visitor
(
	uuid String,
	user_id Nullable(int),
	user_agent Nullable(String),
	remote_addr Nullable(String),
	client_id Nullable(String),
	at DateTime default now()
)
engine = Memory;

create table skymarket_user_open_product
(
    uuid       String,
    product_id Int32,
    shop_id    Int32,
    at         DateTime default now()
)
    engine = Memory;


*/