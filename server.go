package main

import (
	"fmt"
	"go.srv/packages/db/clickhouse"
	"go.srv/packages/db/redis"
	"go.srv/packages/handlers/middleware/auth"
	"go.srv/packages/handlers/register"
	//productOpen "go.srv/packages/handlers/statistic/skymarket/product/open"
	authH "go.srv/packages/handlers/auth"
	"go.srv/packages/listener"
	"net/http"
)

func main() {

	//устанавливаем соединение с redis, иначе panic
	redis.Client = redis.InitClient()
	//устанавливаем соединение с clickhouse
	clickhouse.Connect = clickhouse.InitConnection()

	http.HandleFunc("/register", register.Handler)
	http.Handle("/auth", auth.Handler(http.HandlerFunc(authH.Handler)))

	go listener.Write()
/*    http.HandleFunc("/product/open/", productOpen.Handler) // Открытие
    //todo: add get & delete (add DEL row) row by userId&productId
    http.HandleFunc("/product/like/", productLike.Handler) // Лайк [GET, POST, DELETE]
*/
    err := http.ListenAndServe(":4321", nil) // задаем слушать порт
    if err != nil {
       fmt.Println(err)
    }
}
